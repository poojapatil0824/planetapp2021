# Planets App using MVVM Design Pattern

	-	I have used `Swift` and `Xcode 12+` to develop this app
	-	This is universal app and deployment target is supported for `iOS 13`
	-	I have implemented `MVVM` design pattern for the project architecture 
	-	To implement the offline use I have used `NSKeyedArchiver` a concrete subclass of NSCoder
	-	Also Implemented `Pull to refresh` functionality to reload the data from server
	-	WebService Unit tests are written 
	
## PlanetAppMVVM
	
	-	Build and run the project in Xcode
	-	For the very first time there will not be any data in the tableview
	-	I have assumed that this will happen after authentication screen
	-	Load the data by `Pull to refresh`
	-	Now if you close the app and reopen it, API call is not made and data saved locally is shown
	- 	Disconnect internet connection and reopen the app, you will see that data is persisted