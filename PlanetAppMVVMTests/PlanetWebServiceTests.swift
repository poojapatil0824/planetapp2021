//
//  PlanetWebServiceTests.swift
//  PlanetAppMVVMTests
//
//  Created by Pooja Awati on 07/04/2021.
//  Copyright © 2021 Pooja Awati. All rights reserved.
//

import XCTest
@testable import PlanetAppMVVM

class PlanetWebServiceTests: XCTestCase {

    let mockPlanetWebService = MockPlanetWebService()

    func testPlanetWebServiceResponse() {
        let expectation = self.expectation(description: "Planet Web Service Parse Expected")
        mockPlanetWebService.fetchPlanetData(){ json, error in

            XCTAssertNil(error)
            guard json != nil else {
                XCTFail()
                return
            }
            do {
                XCTAssertNotNil(json)
                expectation.fulfill()
            }
        }
        self.waitForExpectations(timeout: 10.0, handler: nil)
    }

}
