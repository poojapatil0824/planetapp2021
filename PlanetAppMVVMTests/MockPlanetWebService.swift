//
//  MockPlanetWebService.swift
//  PlanetAppMVVMTests
//
//  Created by Pooja Awati on 07/04/2021.
//  Copyright © 2021 Pooja Awati. All rights reserved.
//

import Foundation
import XCTest
@testable import PlanetAppMVVM

class MockPlanetWebService {
    var shouldServiceReturnError = false
    var planetServiceRequestWasCalled = false
    var mockJSONData: Planet?

    enum MockPlanetServiceError: Error{
        case planet
    }
    func reset(){
        shouldServiceReturnError = false
        planetServiceRequestWasCalled = false
    }
    convenience init(){
        self.init(false)
    }
    init(_ shouldServiceReturnError: Bool) {
        self.shouldServiceReturnError = shouldServiceReturnError
        self.getMockJSONData()
    }

    func getMockJSONData(){
        if let path = Bundle.main.path(forResource: "data", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: [])
                let decoder = JSONDecoder()
                self.mockJSONData = try decoder.decode(Planet.self, from: data)
            } catch {
                print("Error loading JSON file")
            }
        } else {
            print("File not found")
        }
    }
}
extension MockPlanetWebService: PlanetWebServiceProtocol {
    func fetchPlanetData(completion: @escaping (Planet?, Error?) -> Void) {
        planetServiceRequestWasCalled = true

        if shouldServiceReturnError {
            completion(nil, MockPlanetServiceError.planet)
        } else {
            completion(self.mockJSONData, nil)
        }
    }
}
