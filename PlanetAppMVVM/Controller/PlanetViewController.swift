//
//  PlanetViewController.swift
//  PlanetAppMVVM
//
//  Created by Pooja Awati on 07/04/2021.
//  Copyright © 2021 Pooja Awati. All rights reserved.
//

import UIKit
import Foundation

class PlanetViewController: UIViewController {
    @IBOutlet weak var planetListTableView: UITableView!

    private var listOfPlanetsArray = [PlanetDetails]()
    private let planetWebService = PlanetWebService()

    var viewModel = PlanetViewModel(planetWebService: PlanetWebService())

    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl(frame: .zero)
        refreshControl.addTarget(self,
                                 action: #selector(refreshPlanetDetails),
                                 for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        registerForNotification()
        planetListTableView.refreshControl = refreshControl
        loadPlanetDetails()
    }

    @objc func refreshPlanetDetails() {
        refreshControl.beginRefreshing()
        //update planet data set asynchronously
        viewModel.getPlanetResults()
    }

    func registerForNotification() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UpdatePlanetDetails,
                                               object: nil,
                                               queue: OperationQueue.main) { _ in
                                                self.refreshControl.endRefreshing()
                                                self.loadPlanetDetails()
        }
    }

    func loadPlanetDetails() {
        guard let listOfPlanetsArray = viewModel.fetchPlanetDetails() else {
            print("planet detials not available")
            self.planetListTableView.reloadData()
            return
        }
        self.listOfPlanetsArray = listOfPlanetsArray
        self.planetListTableView.reloadData()
        planetListTableView.tableFooterView = UIView(frame: .zero)

    }
}

extension PlanetViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfPlanetsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlanetCell") as! PlanetListTableViewCell
        cell.planetNameLabel?.text = listOfPlanetsArray[indexPath.row].name
        return cell
    }
}

