//
//  PlanetDataStore.swift
//  PlanetAppMVVM
//
//  Created by Pooja Awati on 07/04/2021.
//  Copyright © 2021 Pooja Awati. All rights reserved.
//

import Foundation

class PlanetDataStore: NSObject {

    static let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first

    static let fileName = "PlanetDetails.data"

    static func store(planetDetails: [PlanetDetails]) {
        guard let documentURL = documentURL else {
            return
        }
        let fileURL = documentURL.appendingPathComponent(fileName)
        let data = try? PropertyListEncoder().encode(planetDetails)
        let success = NSKeyedArchiver.archiveRootObject(data!, toFile: fileURL.path)
        print(success ? "Successfully Saved" : "Save Failed")
    }

    static func retrievePlanetDetails() -> [PlanetDetails]? {
        guard let documentURL = documentURL else {
            return nil
        }
        let fileURL = documentURL.appendingPathComponent(fileName)
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: fileURL.path) as? Data else {
            return nil
        }
        let planetDetails = try? PropertyListDecoder().decode([PlanetDetails].self, from: data)
        return planetDetails

    }

}
