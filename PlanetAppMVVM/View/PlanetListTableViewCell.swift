//
//  PlanetListTableViewCell.swift
//  PlanetAppMVVM
//
//  Created by Pooja Awati on 07/04/2021.
//  Copyright © 2021 Pooja Awati. All rights reserved.
//

import Foundation
import UIKit

class PlanetListTableViewCell: UITableViewCell {
    @IBOutlet weak var planetNameLabel: UILabel!

}
