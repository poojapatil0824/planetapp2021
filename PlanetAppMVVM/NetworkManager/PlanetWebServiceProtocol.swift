//
//  PlanetWebServiceProtocol.swift
//  PlanetAppMVVM
//
//  Created by Pooja Awati on 07/04/2021.
//  Copyright © 2021 Pooja Awati. All rights reserved.
//

import Foundation

protocol PlanetWebServiceProtocol {
    func fetchPlanetData(completion: @escaping (_ planet : Planet?, _ error: Error?) -> Void)
}
