//
//  Planet.swift
//  PlanetAppMVVM
//
//  Created by Pooja Awati on 07/04/2021.
//  Copyright © 2021 Pooja Awati. All rights reserved.
//

import Foundation

struct Planet: Codable {
    let count: Int?
    let next: URL?
    let previous: String?
    let results: [PlanetDetails]
}

struct PlanetDetails {
    let name: String?
    let rotationPeriod: String?
    let orbitalPeriod: String?
    let diameter: String?
    let climate: String?
    let gravity: String?
    let terrain: String?
    let surfaceWater: String?
    let population: String?
    let residents: [URL]?
    let films: [URL]?
    let created: String?
    let edited: String?
    let url: URL?
}

extension PlanetDetails: Codable {

    enum CodingKeys: String, CodingKey {
        case name
        case rotationPeriod = "rotation_period"
        case orbitalPeriod = "orbital_period"
        case diameter = "diameter"
        case climate = "climate"
        case gravity = "gravity"
        case terrain = "terrain"
        case surfaceWater = "surface_water"
        case population = "population"
        case residents = "residents"
        case films = "films"
        case created = "created"
        case edited = "edited"
        case url = "url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        rotationPeriod = try values.decodeIfPresent(String.self, forKey: .rotationPeriod)
        orbitalPeriod = try values.decodeIfPresent(String.self, forKey: .orbitalPeriod)
        diameter = try values.decodeIfPresent(String.self, forKey: .diameter)
        climate = try values.decodeIfPresent(String.self, forKey: .climate)
        gravity = try values.decodeIfPresent(String.self, forKey: .gravity)
        terrain = try values.decodeIfPresent(String.self, forKey: .terrain)
        surfaceWater = try values.decodeIfPresent(String.self, forKey: .surfaceWater)
        population = try values.decodeIfPresent(String.self, forKey: .population)
        residents = try values.decodeIfPresent([URL].self, forKey: .residents)
        films = try values.decodeIfPresent([URL].self, forKey: .films)
        created = try values.decodeIfPresent(String.self, forKey: .created)
        edited = try values.decodeIfPresent(String.self, forKey: .edited)
        url = try values.decodeIfPresent(URL.self, forKey: .url)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(rotationPeriod, forKey: .rotationPeriod)
        try container.encode(orbitalPeriod, forKey: .orbitalPeriod)
        try container.encodeIfPresent(diameter, forKey: .diameter)
        try container.encodeIfPresent(climate, forKey: .climate)
        try container.encodeIfPresent(gravity, forKey: .gravity)
        try container.encode(terrain, forKey: .terrain)
        try container.encodeIfPresent(surfaceWater, forKey: .surfaceWater)
        try container.encodeIfPresent(population, forKey: .population)
        try container.encodeIfPresent(residents, forKey: .residents)
        try container.encodeIfPresent(films, forKey: .films)
        try container.encode(created, forKey: .created)
        try container.encodeIfPresent(edited, forKey: .edited)
        try container.encodeIfPresent(url, forKey: .url)
    }

}
